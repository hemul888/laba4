interface ITracker<T> {
    fun isStopped(): Boolean
    fun next()
    fun get(): T?
    fun set(value: T?)
}
