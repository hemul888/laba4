private val pN = P(0, -1)
private val pNE = P(1, -1)
private val pE = P(1, 0)
private val pSE = P(1, 1)
private val pS = P(0, 1)
private val pSW = P(-1, 1)
private val pW = P(-1, 0)
private val pNW = P(-1, -1)

enum class Direction {
    N {
        override fun reverse() = S
        override fun turn(clockwise: Boolean) = if (clockwise) NE else NW
        override fun getP() = pN
    },
    NE {
        override fun reverse() = SW
        override fun turn(clockwise: Boolean) = if (clockwise) E else N
        override fun getP() = pNE
    },
    E {
        override fun reverse() = W
        override fun turn(clockwise: Boolean) = if (clockwise) SE else NE
        override fun getP() = pE
    },
    SE {
        override fun reverse() = NW
        override fun turn(clockwise: Boolean) = if (clockwise) S else E
        override fun getP() = pSE
    },
    S {
        override fun reverse() = N
        override fun turn(clockwise: Boolean) = if (clockwise) SW else SE
        override fun getP() = pS
    },
    SW {
        override fun reverse() = NE
        override fun turn(clockwise: Boolean) = if (clockwise) W else S
        override fun getP() = pSW
    },
    W {
        override fun reverse() = E
        override fun turn(clockwise: Boolean) = if (clockwise) NW else SW
        override fun getP() = pW
    },
    NW {
        override fun reverse() = SE
        override fun turn(clockwise: Boolean) = if (clockwise) N else W
        override fun getP() = pNW
    };

    abstract fun reverse(): Direction
    abstract fun turn(clockwise: Boolean = true): Direction
    abstract fun getP(): P
}

class MatrixByLineTrackerBuilder<T> {
    var matrix: IMatrix<T>? = null
    var startP: P? = null
    var direction: Direction? = null
    var firstTurnClockwise: Boolean? = null
    fun build(): MatrixByLineTracker<T> {
        assert(matrix != null) { "matrix not set" }
        assert(startP != null) { "startP not set" }
        assert(matrix!!.inBounds(startP!!)) { "startP is not in matrix bounds" }
        assert(direction != null) { "direction not set" }
        assert(firstTurnClockwise != null) { "firstTurnClockwise not set" }
        return MatrixByLineTracker<T>(matrix!!, startP!!, direction!!, firstTurnClockwise!!)
    }

    fun matrix(value: IMatrix<T>) = apply { matrix = value }
    fun startP(value: P) = apply { startP = value }
    fun direction(value: Direction) = apply { direction = value }
    fun firstTurnClockwise(value: Boolean) = apply { firstTurnClockwise = value }
}

abstract class MatrixTracker<T>(protected val matrix: IMatrix<T>) : ITracker<T> {
    protected var stopped: Boolean = false
    protected var cell: P = P(0, 0)

    override fun isStopped(): Boolean {
        return stopped
    }

    override fun get(): T? {
        if (stopped)
            throw IllegalCallerException("tracker is stopped")
        return matrix[cell]
    }

    override fun set(value: T?) {
        if (stopped)
            throw IllegalCallerException("tracker is stopped")
        matrix[cell] = value
    }
}

class MatrixByLineTracker<T> (
    matrix: IMatrix<T>,
    cell: P,
    private var direction: Direction,
    private var turnClockwise: Boolean,
) : MatrixTracker<T>(matrix) {

    init {
        this.cell = cell
    }

    private fun tryTurn(): P? {
        var d = direction.turn(turnClockwise)
        while (d != direction.reverse()) {
            val result = cell + d.getP()
            if (matrix.inBounds(result)) { // точка продолжения найдена
                return result
            }
            d = d.turn(turnClockwise) // продолжаем поворачиваться
        }
        return null
    }

    override fun next() {
        if (stopped)
            throw IllegalCallerException("tracker is stopped")

        var nextCell: P? = cell + direction.getP()
        if (matrix.inBounds(nextCell!!)) {
            cell = nextCell
        }
        else { // дошли до края. поворачиваем
            nextCell = tryTurn()
            if (nextCell == null) { // не найдена точка продолжения
                stopped = true
                return
            } else {
                direction = direction.reverse() // меняем направление движения
                turnClockwise = !turnClockwise // меняем направление поворота
                cell = nextCell
            }
        }
    }
}