fun <T> printMatrix(m: IMatrix<T>) {
    for (row in 0 until m.width) {
        for (col in 0 until m.height) {
            print("${m[row, col]} ")
        }
        println()
    }
}

typealias CellValue = Int

fun main() {
    val m = createSquareMatrix<CellValue>(4)

    val tracker = MatrixByLineTrackerBuilder<Int>()
        .matrix(m)
        .startP(P(0, 0))
        .direction(Direction.NE)
        .firstTurnClockwise(true)
        .build()

    var i = 0
    while (!tracker.isStopped()) {
        tracker.set(i++)
        tracker.next()
    }
    printMatrix(m)
}
