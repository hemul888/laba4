class P(val x: Int, val y: Int) {

    operator fun plus(value: P): P {
        return P(this.x + value.x, this.y + value.y)
    }

    override fun toString(): String {
        return "($x, $y)"
    }
}

interface IMatrix<T> {
    val height: Int
    val width: Int

    operator fun get(row: Int, col: Int): T?
    operator fun get(p: P): T?
    operator fun set(row: Int, col: Int, value: T?)
    operator fun set(p: P, value: T?)

    fun inBounds(row: Int, col: Int) = (row in 0 until height) && (col in 0 until width)
    fun inBounds(p: P) = inBounds(p.y, p.x)
}

class Matrix<T>(override val height: Int, override val width: Int) : IMatrix<T> {
    init {
        assert(height > 0) { "height cant be $height" }
        assert(width > 0) { "width cant be $width" }
    }

    private val cells = Array<Any?>(height * width) { null } as Array<T?>

    private fun getCellIndex(row: Int, col: Int): Int {
        assert(row in 0 until height && col in 0 until width) { "cell [$row, $col] is out of borders for matrix[$width, $height]"}
        return width * row + col
    }

    override fun get(row: Int, col: Int): T? {
        return cells[getCellIndex(row, col)]
    }

    override fun get(p: P): T? {
        return get(p.y, p.x)
    }

    override fun set(row: Int, col: Int, value: T?) {
        cells[getCellIndex(row, col)] = value
    }

    override fun set(p: P, value: T?) {
        set(p.y, p.x, value)
    }

}

fun <T> createSquareMatrix(size: Int): IMatrix<T> {
    return Matrix<T>(size, size)
}
