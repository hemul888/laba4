import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.assertThrows

typealias Type = Double

internal class MatrixByLineTrackerTest {

    private val m = Matrix<Type>(1, 1)

    @Test
    operator fun next() {
        val t = MatrixByLineTracker<Type>(m, P(0, 0), Direction.N, true)
        assertDoesNotThrow { t.next() }
        val e = assertThrows<IllegalCallerException> {
            t.next()
        }
        assertEquals(e.message, "tracker is stopped")
    }

    @Test
    fun isStopped() {
        val t = MatrixByLineTracker<Type>(m, P(0, 0), Direction.N, true)
        t.next()
        assertEquals(t.isStopped(), true)
    }

    @Test
    fun get() {
        val t = MatrixByLineTracker<Type>(m, P(0, 0), Direction.N, true)
        assertEquals(t.get(), null)
        val e = assertThrows<IllegalCallerException> {
            t.next()
            t.get()
        }
        assertEquals(e.message, "tracker is stopped")
    }

    @Test
    fun set() {
        val t = MatrixByLineTracker<Type>(m, P(0, 0), Direction.N, true)
        val value = 10.0
        t.set(value)
        assertEquals(m[0, 0], value)
        val e = assertThrows<IllegalCallerException> {
            t.next()
            t.set(value)
        }
        assertEquals(e.message, "tracker is stopped")
    }

}