import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class MainKtTest {

    @Test
    fun printMatrix() {
        assertDoesNotThrow {
            val m = createSquareMatrix<Byte>(10)
            printMatrix(m)
        }
    }

    @Test
    fun mainBody() {
        assertDoesNotThrow {
            main()
        }
    }

}