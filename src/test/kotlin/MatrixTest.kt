import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class MatrixTest {

    private val height = 3
    private val width = 4
    private val m = Matrix<Int>(height, width)

    @Test
    fun get() {
        m[1, 1] = 100
        assertEquals(m[1, 1], 100)
    }

    @Test
    fun testGet() {
        m[1, 1] = 100
        assertEquals(m[P(1, 1)], 100)
    }

    @Test
    fun set() {
        m[0, 0] = 200
        assertEquals(m[0, 0], 200)
    }

    @Test
    fun testSet() {
        m[P(0, 1)] = 300
        assertEquals(m[P(0, 1)], 300)
    }

    @Test
    fun getHeight() {
        assertEquals(m.height, height)
    }

    @Test
    fun getWidth() {
        assertEquals(m.width, width)
    }
}