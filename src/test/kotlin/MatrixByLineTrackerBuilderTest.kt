import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.assertThrows

typealias CellValue = Int

internal class MatrixByLineTrackerBuilderTest {

    private val size = 4
    @Test
    fun build() {
        val m = createSquareMatrix<CellValue>(size)
        assertDoesNotThrow {
            val tracker = MatrixByLineTrackerBuilder<CellValue>()
                .matrix(m)
                .startP(P(m.width - 1, m.height - 1))
                .direction(Direction.NE)
                .firstTurnClockwise(false)
                .build()
            tracker.next()
        }
    }

    @Test
    fun matrix() {
        val m = createSquareMatrix<CellValue>(size)
        val tracker = MatrixByLineTrackerBuilder<CellValue>()
            .matrix(m)
        assertEquals(tracker.matrix, m)
    }

    @Test
    fun startP() {
        val p = P(0, 0)
        val tracker = MatrixByLineTrackerBuilder<CellValue>()
            .startP(p)
        assertEquals(tracker.startP, p)
    }

    @Test
    fun direction() {
        val d = Direction.S
        val tracker = MatrixByLineTrackerBuilder<CellValue>()
            .direction(d)
        assertEquals(tracker.direction, d)
    }

    @Test
    fun firstTurnClockwise() {
        val t = false
        val tracker = MatrixByLineTrackerBuilder<CellValue>()
            .firstTurnClockwise(t)
        assertEquals(tracker.firstTurnClockwise, t)
    }
}