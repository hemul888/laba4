import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class DirectionTest {

    @Test
    fun reverse() {
        for (d in Direction.values())
            assertEquals(d.reverse().reverse(), d)
    }

    @Test
    fun turn() {
        for (d in Direction.values())
            assertEquals(d.turn(true).turn(false), d)
    }

    @Test
    fun getP() {
        var p = P(0, 0)
        for (d in Direction.values())
            p += d.getP()
        assertEquals(p.x, 0)
        assertEquals(p.y, 0)
    }
}