import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class MatrixKtTest {

    @Test
    fun createSquareMatrix() {
        val size = 6
        val m = createSquareMatrix<String>(size)
        assertEquals(m.height, size)
        assertEquals(m.width, size)
    }
}