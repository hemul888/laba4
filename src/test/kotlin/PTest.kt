import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class PTest {

    @Test
    fun plus() {
        val p = P(10, 11) + P(12, 13)
        assertEquals(10 + 12, p.x)
        assertEquals(11 + 13, p.y)
    }

    @Test
    fun testToString() {
        assertEquals(P(12, 13).toString(), "(12, 13)")
    }
}